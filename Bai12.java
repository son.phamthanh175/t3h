package String;

public class Bai12 {
    public static void main(String[] args) {
        Bai12();
    }
    static void Bai12(){
        String str1 = "hello";
        char[] charArray = str1.toCharArray();
        for(int i = 0; i < charArray.length; i++) {
            if(charArray[i] >= 97 && charArray[i] <= 122){
                charArray[i] -= 32;
            }
        }
        str1 = String.valueOf(charArray);
        System.out.println(str1);
    }
}
